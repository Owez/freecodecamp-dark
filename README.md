# freeCodeCamp Dark

## About

freeCodeCamp Dark is a small theme that has been tested on Firefox that makes the https://guide.freecodecamp.org/ page dark to improve readability.

All respective trademarks go to the freeCodeCamp charity.
